#pragma once

#include <stddef.h>
#include <stdint.h>

void gomocu_rnd_next_bytes(uint8_t* bytes, size_t size);
