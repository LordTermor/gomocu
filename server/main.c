#include "server.h"

#include "logger.h"
#include "config.h"
#include "state.h"

#include <signal.h>
#include <unistd.h>
#include <sys/stat.h>

#include <string.h>
#include <stdlib.h>


void daemonize()
{
    pid_t pid;

    pid = fork();

    if (pid < 0)
        exit(1);

    if (pid > 0)
        exit(0);

    if (setsid() < 0)
        exit(1);

    signal(SIGCHLD, SIG_IGN);

    signal(SIGHUP, SIG_IGN);

    pid = fork();

    if (pid < 0)
        exit(1);

    if (pid > 0)
        exit(0);


    chdir("/");

    int x;
    for (x = sysconf(_SC_OPEN_MAX); x>=0; x--)
    {
        close (x);
    }

    umask(0);


}


gomocu_server_master* main_server;

void gomocu_server_trap_sigterm(int sig){
    gomocu_server_master_stop(main_server);
    gomocu_server_master_free(main_server);

    _exit(0);
}

void gomocu_server_trap_sighup(int sig){

    if(main_server->state->stage!=gomocu_server_state_game_stage_stopped){
        return;
    }
    gomocu_server_master_reset(main_server);

    gomocu_server_master_init(main_server);
}

int main(int argc, char* argv[]){

    if(argc>1 && (strcmp(argv[1], "-d")==0))
        daemonize();

    gomocu_server_logger_init();
    LOGGER_ASSERT((gomocu_server_config_init()>=0), "Error opening config");

    char* portstr = malloc(14);
    strcpy(portstr, "0.0.0.0:xxxxx");
    int readn = gomocu_server_config_read("port", portstr+8,5);

    if(readn==0){
        gomocu_server_config_write("port", "5000");
        strcpy(&portstr[8], "5000");
    }

    main_server
            = gomocu_server_master_new(portstr);

    signal(SIGTERM, gomocu_server_trap_sigterm);
    signal(SIGHUP, gomocu_server_trap_sighup);

    gomocu_server_master_start(main_server);

    gomocu_server_master_free(main_server);

    return 0;

}
