#pragma once

#include "worker.h"

struct _gomocu_server_executor {
    gomocu_server_worker parent_instance;


};
typedef struct _gomocu_server_executor gomocu_server_executor;

gomocu_server_executor* gomocu_server_executor_new(int shmemfd, int semaphore);
void gomocu_server_executor_init(gomocu_server_executor* self, int shmemfd, int semaphore);

void gomocu_server_executor_free(gomocu_server_executor* self);
