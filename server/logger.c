#include "logger.h"

//syscalls
#include <unistd.h>
#include <time.h>
#include <fcntl.h>

//stdlib
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

static gomocu_server_logger* g_logger;

void gomocu_server_logger_init(void){
    g_logger = gomocu_server_logger_new();
}

gomocu_server_logger * gomocu_server_logger_new()
{
    gomocu_server_logger* result = malloc(sizeof(gomocu_server_logger));

    result->stdout_fd = STDOUT_FILENO;

    time_t now = time(NULL);
    char* ctime_no_nl = strtok(ctime(&now), "\n");

    char logfilename[255];
    sprintf(logfilename, "%s-%s.log", "gomocu-server", ctime_no_nl);


    result->logfile_fd = open(logfilename, O_CREAT|O_TRUNC|O_WRONLY, 0644);

    if(result->logfile_fd<0){
        char* error = "Cannot open log file";
        write( result->stdout_fd, error, strlen(error) );
    }

    return result;

}

void gomocu_server_logger_log(gomocu_server_logger* self, gomocu_server_logger_categories category, const char *message)
{
    if(!self){
        return;
    }

    if(!message || strlen(message)==0){
        return;
    }

    char* category_string;

    switch(category){
        case LOGGER_CATEGORY_ERROR:
            category_string = "ERROR";
            break;
        case LOGGER_CATEGORY_INFO:
            category_string = "INFO";
            break;
        default:
            category_string = "UNKNOWN";
            break;
    }

    char formatted_string[1024];

    time_t now = time(NULL);
    char* ctime_no_nl = strtok(ctime(&now), "\n");

    sprintf(formatted_string, "[%s][%s] %s\n", ctime_no_nl, category_string, message);

    if(self->logfile_fd >= 0){
        write(self->logfile_fd, formatted_string, strlen(formatted_string));
    }
    if(self->stdout_fd >= 0){
        write(self->stdout_fd, formatted_string, strlen(formatted_string));
    }
}


void gomocu_server_logger_log_format(gomocu_server_logger *self, gomocu_server_logger_categories category, const char *format, ...)
{
    char buffer[1024];
    va_list args;
    va_start(args, format);

    vsnprintf(buffer, 1024, format, args);

    gomocu_server_logger_log(self, category, buffer);
}


gomocu_server_logger *gomocu_server_logger_instance()
{
    return g_logger;
}
