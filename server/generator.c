#include "generator.h"

#include <stdlib.h>
#include <string.h>

#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#include "state.h"
#include "logger.h"

void gomocu_server_generator_start(gomocu_server_generator *self)
{
    LOG_INFO("Generator started");

    while(1){
        switch (self->parent_instance.state->stage) {
        case gomocu_server_state_game_stage_preparing: {
            gomocu_server_worker_acquire_state(&self->parent_instance);

            if(self->parent_instance.state->max_field_h <= 0
                    || self->parent_instance.state->max_field_w <= 0){
                LOG_INFO("Generator: Generating field...");

                for(int i =0;i<FIELD_SIZE*FIELD_SIZE;i++){
                    self->parent_instance.state->field[i] = gomocu_server_state_cell_state_none;
                }

                self->parent_instance.state->max_field_w = self->parent_instance.state->max_field_h = FIELD_SIZE;

                LOG_INFO("Generator: Generation finished");
            }
            gomocu_server_worker_release_state(&self->parent_instance);

        }
        default:
            usleep(300);
            continue;
        }
    }
}


gomocu_server_generator *gomocu_server_generator_new(int shmemfd, int semaphore)
{
    gomocu_server_generator* result = malloc(sizeof(gomocu_server_generator));

    gomocu_server_generator_init(result, shmemfd, semaphore);

    return result;
}

void gomocu_server_generator_init(gomocu_server_generator *self, int shmemfd, int semaphore)
{
    gomocu_server_worker_init(&self->parent_instance, shmemfd, semaphore);
    self->parent_instance.start = gomocu_server_generator_start;
}

void gomocu_server_generator_free(gomocu_server_generator *self)
{
    gomocu_server_worker_reset(&self->parent_instance);

    free(self);
}
