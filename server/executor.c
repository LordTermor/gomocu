#include "executor.h"

#include "state.h"
#include "logger.h"

#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <unistd.h>

void gomocu_server_executor_start(gomocu_server_executor *self)
{
    LOG_INFO("Executor started");
    while(1){
        usleep(300);
        gomocu_server_worker_acquire_state(&self->parent_instance);

        if(self->parent_instance.state->selectedX < 0
                || self->parent_instance.state->selectedY < 0){
            continue;
        }
        if(self->parent_instance.state->stage != gomocu_server_state_game_stage_firstplayer_turn
                && self->parent_instance.state->stage != gomocu_server_state_game_stage_secondplayer_turn){
            self->parent_instance.state->selectedX = -1;
            self->parent_instance.state->selectedY = -1;
            continue;
        }

        if(!gomocu_server_state_set(self->parent_instance.state, self->parent_instance.state->selectedX, self->parent_instance.state->selectedY,
                                self->parent_instance.state->stage == gomocu_server_state_game_stage_firstplayer_turn
                                ?gomocu_server_state_cell_state_first
                               :gomocu_server_state_cell_state_second)){

            self->parent_instance.state->selectedX = -1;
            self->parent_instance.state->selectedY = -1;
            strcpy(self->parent_instance.state->stage == gomocu_server_state_game_stage_firstplayer_turn
                   ?self->parent_instance.state->player_one_rsp
                  :self->parent_instance.state->player_two_rsp, "Wrong turn! Try again.");
            continue;
        }

        int current_player = self->parent_instance.state->stage == gomocu_server_state_game_stage_firstplayer_turn?1:2;

        char buffer[1024];

        snprintf(buffer, 1024, "TURN %d %d %d", current_player, self->parent_instance.state->selectedX,
                 self->parent_instance.state->selectedY);

        gomocu_server_state_turn(self->parent_instance.state);


        //TODO: Win condition
        int counter = 0;

        LOG_INFO("Check win horizontal");

        for(int x = fmax(0, self->parent_instance.state->selectedX-5)+1;
            x < fmin(self->parent_instance.state->max_field_w, self->parent_instance.state->selectedX+5)
            && counter<4;x++){
            if(  gomocu_server_state_get(self->parent_instance.state, x, self->parent_instance.state->selectedY)
                 != gomocu_server_state_cell_state_none &&
                    gomocu_server_state_get(self->parent_instance.state, x, self->parent_instance.state->selectedY)
                    == gomocu_server_state_get(self->parent_instance.state, x-1, self->parent_instance.state->selectedY)){
                counter++;
            } else {
                counter = 0;
            }
        }
        LOG_INFO_F("Max marks in a row: %d", counter+1);

        if(counter<4){

            LOG_INFO("Check win vertical");
            counter = 0;
            for(int y = fmax(0, self->parent_instance.state->selectedY-5)+1;
                y < fmin(self->parent_instance.state->max_field_h, self->parent_instance.state->selectedY+5)
                && counter<4;y++){
                if( gomocu_server_state_get(self->parent_instance.state, self->parent_instance.state->selectedX, y)
                        != gomocu_server_state_cell_state_none &&
                 gomocu_server_state_get(self->parent_instance.state, self->parent_instance.state->selectedX, y)
                        == gomocu_server_state_get(self->parent_instance.state, self->parent_instance.state->selectedX, y-1)){
                    counter++;

                } else {
                    counter = 0;
                }
            }
        }
        LOG_INFO_F("Max marks in a row: %d", counter+1);

        if(counter>=4){
            strcpy(self->parent_instance.state->stage == gomocu_server_state_game_stage_firstplayer_turn
                   ?self->parent_instance.state->player_one_rsp
                  :self->parent_instance.state->player_two_rsp, "You won!");

            self->parent_instance.state->stage = gomocu_server_state_game_stage_stopped;
            gomocu_server_worker_release_state(&self->parent_instance);

            continue;
        }

        self->parent_instance.state->selectedX = -1;
        self->parent_instance.state->selectedY = -1;


        self->parent_instance.state->stage =
                self->parent_instance.state->stage == gomocu_server_state_game_stage_firstplayer_turn
                ?gomocu_server_state_game_stage_secondplayer_turn
               :gomocu_server_state_game_stage_firstplayer_turn;

        LOG_INFO("Next player turn!");

        strcpy(self->parent_instance.state->player_one_rsp, buffer);
        strcpy(self->parent_instance.state->player_two_rsp, buffer);



        gomocu_server_worker_release_state(&self->parent_instance);
    }
}

gomocu_server_executor *gomocu_server_executor_new(int shmemfd, int semaphore)
{
    gomocu_server_executor* result = malloc(sizeof(gomocu_server_executor));

    gomocu_server_executor_init(result, shmemfd, semaphore);

    return result;
}

void gomocu_server_executor_init(gomocu_server_executor *self, int shmemfd, int semaphore)
{
    gomocu_server_worker_init(&self->parent_instance, shmemfd, semaphore);

    self->parent_instance.start = gomocu_server_executor_start;
}

void gomocu_server_executor_free(gomocu_server_executor *self)
{
    gomocu_server_worker_reset(&self->parent_instance);
    free(self);
}
