#pragma once

#include <unistd.h>

enum _gomocu_server_logger_categories {
    LOGGER_CATEGORY_ERROR, LOGGER_CATEGORY_INFO
};
typedef enum _gomocu_server_logger_categories gomocu_server_logger_categories;

struct _gomocu_server_logger{

    int stdout_fd;
    int logfile_fd;

};

typedef struct _gomocu_server_logger gomocu_server_logger;

gomocu_server_logger* gomocu_server_logger_new();

void gomocu_server_logger_log( gomocu_server_logger* self, gomocu_server_logger_categories category, const char* message );
void gomocu_server_logger_log_format( gomocu_server_logger* self, gomocu_server_logger_categories category, const char* format, ... );
void gomocu_server_logger_init(void);
gomocu_server_logger* gomocu_server_logger_instance();

#define LOG_ERROR(MESSAGE) \
    gomocu_server_logger_log(gomocu_server_logger_instance(), LOGGER_CATEGORY_ERROR, MESSAGE);

#define LOG_INFO(MESSAGE) \
    gomocu_server_logger_log(gomocu_server_logger_instance(), LOGGER_CATEGORY_INFO, MESSAGE);

#define LOG_ERROR_F(FORMAT, ...) \
    gomocu_server_logger_log_format(gomocu_server_logger_instance(), LOGGER_CATEGORY_ERROR, FORMAT, __VA_ARGS__)


#define LOG_INFO_F(FORMAT, ...) \
    gomocu_server_logger_log_format(gomocu_server_logger_instance(), LOGGER_CATEGORY_INFO, FORMAT, __VA_ARGS__)


#define LOGGER_ASSERT(PREDICATE, MESSAGE) \
    if (!PREDICATE) {\
        LOG_ERROR(MESSAGE); \
        _exit(1); \
    }

#define LOGGER_ASSERT_SYSCALL(EXPRESSION, MESSAGE) \
    if (EXPRESSION == -1) {\
        LOG_ERROR(MESSAGE); \
        LOG_ERROR(strerror(errno)); \
        _exit(1); \
    }
