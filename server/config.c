#include "config.h"

#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

int fd;

int gomocu_server_config_init()
{
    fd = open("server.conf", O_RDWR|O_CREAT, 0644);

    return fd>=0;
}

char* read_line(int fd, int* size){
    char n;
    char* result = NULL;
    int r = read(fd, &n, 1);
    if(r<=0 || n=='\n'){
        return result;
    }
    *size = 1;
    result=malloc(*size);
    result[0] = n;

    while(read(fd, &n, 1) > 0 && n!='\n'){
        (*size)++;
        result=realloc(result, *size);
        result[*size - 1] = n;
    }
    result = realloc(result, *size+1);
    result[*size] = 0;
    return result;
}

int find_pos(const char* key){
    lseek(fd, 0, SEEK_SET);
    char* a = NULL;
    int pos = 0;
    int line_len = 0;
    while((a = read_line(fd,
                         &line_len))){
        char* tmp = a;
        strtok(tmp, ":");

        if(strcmp(a, key)==0){
            free(a);

            return pos;
        }
        pos += line_len;
        line_len = 0;
        free(a);
    }

    return -1;

}
void shift_data(int fd, int starting_point, int length){
    if(length==0){
        return;
    }

    struct stat st;
    fstat(fd, &st);
    if(length>0){
        lseek(fd, 0, SEEK_END);

        for(int i = st.st_size-1;i>starting_point;i--){
            char buf;
            read(fd, &buf,1);
            lseek(fd, length-1, SEEK_CUR);
            write(fd, &buf, 1);

            lseek(fd, -(2+length) , SEEK_CUR);
        }
    } else {
        lseek(fd, starting_point, SEEK_SET);
        length = -length;

        for(int i = starting_point; i < st.st_size;i++){
            char buf;
            read(fd, &buf,1);
            lseek(fd, -(length+1), SEEK_CUR);
            write(fd, &buf, 1);

            lseek(fd, length , SEEK_CUR);
        }
        ftruncate(fd, st.st_size-length);
    }
}
void gomocu_server_config_write(const char *key, const char *value)
{
    int pos = find_pos(key);
    int size = 0;
    if(pos>-1){
        lseek(fd, pos, SEEK_SET);
        char* line = read_line(fd, &size );
        char* tmp = line;

        line += strlen(key)+1;

        int shift_size = strlen(value) - strlen(line);
        int shift_start_point = pos + strlen(key) + 2 + strlen(line);

        shift_data(fd, shift_start_point, shift_size);

        lseek(fd, pos+strlen(key)+1, SEEK_SET);
        write(fd, value, strlen(value));
        write(fd, "\n", 1);

        free(tmp);
    } else {
        lseek(fd, 0, SEEK_END);
        char keyvalue[1024];
        sprintf(keyvalue,
                "%s:%s\n",
                key, value);

        write(fd, keyvalue, strlen(keyvalue));
    }
}

int gomocu_server_config_read(const char *key, char *value, size_t value_size)
{
    int pos = find_pos(key);
    if(pos<0){
        return 0;
    }
    lseek(fd, pos, SEEK_SET);
    int line_len = 0;
    char* line = read_line(fd, &line_len);
    if(!line){
        return 0;
    }
    char* tmp = line;
    line+=strlen(key)+1;
    int n = strlen(value);

    if(n>value_size){
        n=value_size;
    }

    strncpy(value, line, n);

    free(tmp);

    return n;
}
