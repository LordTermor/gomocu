#include "server.h"

#include <unistd.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <errno.h>
#include <signal.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/shm.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <poll.h>

#include <stdio.h>
#include <string.h>

#include "logger.h"
#include "arbitrator.h"
#include "generator.h"
#include "executor.h"
#include "rnd.h"
#include "state.h"

extern int errno;

gomocu_server_master *gomocu_server_master_new(const char *endpoint)
{
    gomocu_server_master* result = malloc(sizeof(gomocu_server_master));
    result->endpoint = malloc(strlen(endpoint));

    strcpy(result->endpoint, endpoint);

    gomocu_server_master_init(result);

    return result;
}

void gomocu_server_master_free(gomocu_server_master *self)
{
    gomocu_server_master_reset(self);

    free(self);
}

void parse_endpoint(const char* endpoint, char** addr, int16_t* port){
    char* tmp = malloc(strlen(endpoint));
    strcpy(tmp,endpoint);

    char* tmpaddr = strtok(tmp, ":");
    *addr = malloc(strlen(tmpaddr));
    strcpy(*addr, tmpaddr);

    *port = atoi(strtok(NULL,":"));

    free(tmp);
}

void gomocu_server_spawn_worker(gomocu_server_worker* parent, void* worker){
    LOG_INFO("Spawning worker...");
    int pid = fork();

    LOGGER_ASSERT_SYSCALL(pid, "Fork failed!");

    if(pid){
        return;
    }

    parent->state = shmat(parent->shmemfd, NULL, 0);
    gomocu_server_worker_start(worker);
}

void gomocu_server_master_init(gomocu_server_master* self){

    self->exit = 0;

    self->semaphore = semget(ftok(".", 'a'), 0, IPC_CREAT|IPC_EXCL);

    self->shfd = shmget(ftok(".", 'b'), sizeof(gomocu_server_state),
                      IPC_CREAT | IPC_EXCL | S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
    LOGGER_ASSERT_SYSCALL(self->shfd, "Error opening shmem");

    self->state = shmat(self->shfd, NULL, 0);

    memset(self->state, 0, sizeof(gomocu_server_state));
    self->state->stage = gomocu_server_state_game_stage_stopped;

    self->arbitrator = gomocu_server_arbitrator_new(self->shfd, self->semaphore);
    self->generator = gomocu_server_generator_new(self->shfd, self->semaphore);
    self->executor = gomocu_server_executor_new(self->shfd, self->semaphore);

    gomocu_server_spawn_worker(&self->arbitrator->parent_instance, self->arbitrator);
    gomocu_server_spawn_worker(&self->generator->parent_instance, self->generator);
    gomocu_server_spawn_worker(&self->executor->parent_instance, self->executor);

    int16_t desired_port;
    char* desired_address;
    parse_endpoint(self->endpoint,
                   &desired_address,
                   &desired_port);

    struct sockaddr_in addr = {0};
    addr.sin_family = AF_INET;
    addr.sin_port = htons(desired_port);
    addr.sin_addr.s_addr = inet_addr(desired_address);

    self->serverfd = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);

    LOGGER_ASSERT_SYSCALL(self->serverfd,
                          "Socket creation error");


    LOGGER_ASSERT_SYSCALL((bind(self->serverfd, (struct sockaddr*) &addr, sizeof(addr))),
                          "Bind error");

    free(desired_address);

}


void gomocu_server_master_reset(gomocu_server_master *self)
{
    signal(SIGQUIT, SIG_IGN);
    kill(0, SIGQUIT);

    close(self->fd1);
    close(self->fd2);

    close(self->serverfd);
    close(self->semaphore);


    shmdt(self->state);
    shmctl(self->shfd, IPC_RMID, NULL);

    gomocu_server_arbitrator_free(self->arbitrator);
    gomocu_server_generator_free(self->generator);
    gomocu_server_executor_free(self->executor);

    gomocu_server_state_free(self->state);
}


struct sembuf wait_semop[2] = {
{0,0,SEM_UNDO},{0,1,SEM_UNDO}
               };

struct sembuf release_semop[1] = {
{0,-1,SEM_UNDO}
};

void acquire_state(gomocu_server_master* self){
    semop(self->semaphore, wait_semop, 2);
}

void release_state(gomocu_server_master* self){
    semop(self->semaphore, release_semop, 1);
}


void gomocu_server_master_start(gomocu_server_master* self)
{
    struct sockaddr_in addr = {0};

    LOGGER_ASSERT_SYSCALL((listen(self->serverfd, 2)),
                          "Listen error");


    socklen_t socklen = sizeof addr;
    LOG_INFO("Server started listening\n");

    char buf[255];

    int fd1 = accept(self->serverfd, &addr, &socklen);
    LOGGER_ASSERT_SYSCALL(fd1, "Accept error");

    char gameid[255];

    acquire_state(self);
    self->state->stage = gomocu_server_state_game_stage_preparing;
    release_state(self);
    while(self->state->stage==gomocu_server_state_game_stage_preparing){
        int msglen = read(fd1, buf, 255 );

        buf[msglen] = '\0';

        if(strncmp(buf, "start ", strlen("start ")) == 0){
            char* id = strtok(buf, " \0");
            id = strtok(NULL, " \0");

            strcpy(gameid, id);

            while(self->state->max_field_h == 0){
                usleep(300);
            }
            acquire_state(self);
            self->state->stage = gomocu_server_state_game_stage_waitforsecondplayer;
            release_state(self);
            strcpy(buf, "Game started!\n");
            write(fd1, buf, strlen(buf));
        } else {
            strcpy(buf, "Wrong command. Write `start <gameid>` to start a new game!\n");
            write(fd1, buf, strlen(buf));
        }
    }

    int fd2 = accept(self->serverfd, &addr, &socklen);
    LOGGER_ASSERT_SYSCALL(fd2, "Accept error");

    while(self->state->stage==gomocu_server_state_game_stage_waitforsecondplayer){
        int msglen = read(fd2, buf, 255 );

        buf[msglen] = '\0';

        if(strncmp(buf, "join ", strlen("join ")) == 0){
            char* id = strtok(buf, " \0");
            id = strtok(NULL, " \0");

            if(strcmp(gameid, id)==0){
                strcpy(buf, "Succsessfully joined game!\n");
                write(fd2, buf, strlen(buf));

                acquire_state(self);
                self->state->stage = gomocu_server_state_game_stage_firstplayer_turn;
                release_state(self);
            } else {
                strcpy(buf, "Game is started but your provided ID is wrong. Try again!\n");
                write(fd2, buf, strlen(buf));
            }

        } else {
            strcpy(buf, "Wrong command. Type `join <gameid>` to connect to the game!\n");
            write(fd2, buf, strlen(buf));
        }
    }

    struct pollfd fds[2];

    fds[0].fd = fd1;
    fds[0].events = POLLIN;

    fds[1].fd = fd2;
    fds[1].events = POLLIN;
    while(1){
        //POLL
        {
        int msglen = 0;

        int ret = poll( fds, 2, 300 );
        LOGGER_ASSERT_SYSCALL(ret, "Error during polling");
            if ( ret != 0 )
        {
            if ( fds[0].revents & POLLIN ){
                fds[0].revents = 0;
                msglen = read(fd1, buf, 255);
                buf[msglen] = '\0';

                acquire_state(self);
                strcpy(self->state->player_one_rq, buf);
                release_state(self);
            }
            if ( fds[1].revents & POLLIN ){
                fds[1].revents = 0;
                msglen = read(fd2, buf, 255);
                buf[msglen] = '\0';

                acquire_state(self);
                strcpy(self->state->player_two_rq, buf);
                release_state(self);
            }
        }
        }
        //HANDLE RESPONSES
        {
            acquire_state(self);

            if(self->state->player_one_rsp[0]!='\0'){
                char buffer[1024] = {0};
                strcpy(buffer, self->state->player_one_rsp);

                buffer[strlen(buffer)] = '\n';

                write(fd1,buffer, strlen(buffer));
                self->state->player_one_rsp[0] = '\0';
            }
            if(self->state->player_two_rsp[0]!='\0'){
                char buffer[1024] = {0};
                strcpy(buffer, self->state->player_two_rsp);

                buffer[strlen(buffer)] = '\n';
                write(fd2, buffer, strlen(buffer));

                self->state->player_two_rsp[0] = '\0';
            }
            release_state(self);
        }
    }
}

void gomocu_server_master_stop(gomocu_server_master *self)
{
    write(self->fd1, "END\n", 4);
    write(self->fd2, "END\n", 4);
}

void gomocu_server_master_start_detached(gomocu_server_master *self)
{
    if(fork()){
        return;
    }
    setsid();
    gomocu_server_master_start(self);
}
