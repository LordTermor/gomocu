#pragma once

#include "worker.h"

struct _gomocu_server_generator {
    gomocu_server_worker parent_instance;
};

typedef struct _gomocu_server_generator gomocu_server_generator;

gomocu_server_generator* gomocu_server_generator_new(int shmemfd, int semaphore);
void gomocu_server_generator_init(gomocu_server_generator* worker, int shmemfd, int semaphore);

void gomocu_server_generator_free(gomocu_server_generator* self);
