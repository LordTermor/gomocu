#include "arbitrator.h"

#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "state.h"
#include "logger.h"

void gomocu_server_arbitrator_free(gomocu_server_arbitrator *self)
{
    gomocu_server_worker_free(&self->parent_instance);
    free(self);
}

gomocu_server_arbitrator *gomocu_server_arbitrator_new(int shmemfd, int semaphore)
{
    gomocu_server_arbitrator* result = malloc(sizeof(gomocu_server_arbitrator));
    gomocu_server_arbitrator_init(result, shmemfd, semaphore);
    return result;
}

///
/// \brief gomocu_server_arbitrator_handle_turn
/// \param self - arbitrator
/// \param player - one corresponds to the first player, two does to the second
///
void gomocu_server_arbitrator_handle_turn(gomocu_server_arbitrator *self, int player){

    gomocu_server_state_game_stage neededstage = player == 1
            ?gomocu_server_state_game_stage_firstplayer_turn
           :gomocu_server_state_game_stage_secondplayer_turn;

    char* request = player == 1
            ?self->parent_instance.state->player_one_rq
           :self->parent_instance.state->player_two_rq;

    char* response = player == 1
            ?self->parent_instance.state->player_one_rsp
           :self->parent_instance.state->player_two_rsp;

    LOG_INFO_F("Current stage is %d", self->parent_instance.state->stage);
    if(self->parent_instance.state->stage != neededstage){
        LOG_INFO_F("Turn during the wrong stage! %d != %d", self->parent_instance.state->stage, neededstage);
        strcpy(response, "Not your turn!");
        request[0] = '\0';
        return;
    }

    if(strncmp(request, "turn ", strlen("turn "))==0){
        char buffer[1024];
        strcpy(buffer, request);
        strtok(buffer, " ");
        char* x = strtok(NULL, " ");
        char* y = strtok(NULL, " ");

        if(x==NULL || y == NULL){
            LOG_INFO("Arbitrator: Coordinates parse failed: One or more of coordinates are not present");

            strcpy(response, "Coordinates parse failed!");
        } else {
           char* lastchar = NULL;
           int xint = strtol(x, &lastchar, 10);
           if(*lastchar!='\0'){
               xint = -1;
           }
           lastchar = NULL;
           int yint =  strtol(y, &lastchar, 10);
           if(*lastchar!='\0'){
               yint = -1;
           }

           if(xint == -1 || yint == -1){
               LOG_INFO("Arbitrator: Coordinates parse failed: Coordinates are not integer numbers");

               strcpy(response, "Coordinates parse failed!");
           } else {
               LOG_INFO_F("Arbitrator: Parse finished, coordinates are: %d, %d", xint, yint);
               self->parent_instance.state->selectedX = xint-1;
               self->parent_instance.state->selectedY = yint-1;
           }
        }
        request[0] = '\0';

    } else {
        LOG_INFO("Arbitrator: Client sent the wrong command");
        strcpy(response, "Wrong command! Expected turn");
        request[0] = '\0';
    }
}

void gomocu_server_arbitrator_start(gomocu_server_arbitrator *self)
{
    LOG_INFO("Arbitrator started");


    while(1){
        gomocu_server_worker_acquire_state(&self->parent_instance);
        if(self->parent_instance.state->player_one_rq[0]!='\0'){

            LOG_INFO_F("Arbitrator: Message from the first player: %s", self->parent_instance.state->player_one_rq);

            gomocu_server_arbitrator_handle_turn(self, 1);

        }
        if(self->parent_instance.state->player_two_rq[0]!='\0'){

            LOG_INFO_F("Arbitrator: Message from the second player: %s", self->parent_instance.state->player_two_rq);

            gomocu_server_arbitrator_handle_turn(self, 2);
        }
        gomocu_server_worker_release_state(&self->parent_instance);

        usleep(300);


    }
}

void gomocu_server_arbitrator_init(gomocu_server_arbitrator *self, int shmemfd, int semaphore)
{
    gomocu_server_worker_init(&self->parent_instance, shmemfd, semaphore);
    self->parent_instance.start = gomocu_server_arbitrator_start;
}
