#include "worker.h"

//own
#include "state.h"

//std
#include <stdlib.h>
#include <string.h>

//syscalls
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>

void gomocu_server_worker_start(gomocu_server_worker* self)
{
    (*self->start)(self);
}

extern struct sembuf wait_semop[2];
extern struct sembuf release_semop[1];

gomocu_server_state* gomocu_server_worker_acquire_state(gomocu_server_worker *self)
{
    semop(self->semaphore, wait_semop, 2);

    return self->state;
}

gomocu_server_worker *gomocu_server_worker_new(int shmemfd, int semaphore)
{
    gomocu_server_worker* result = malloc(sizeof(gomocu_server_worker));

    memset(result, 0, sizeof(*result));

    gomocu_server_worker_init(result, shmemfd, semaphore);

    return result;
}

void gomocu_server_worker_init(gomocu_server_worker *self, int shmemfd, int semaphore)
{
    self->shmemfd = shmemfd;
    self->semaphore = semaphore;
    self->state = NULL;
    self->start = NULL;
}


void gomocu_server_worker_reset(gomocu_server_worker *self)
{
    close(self->semaphore);
}

void gomocu_server_worker_free(gomocu_server_worker *self)
{
    gomocu_server_worker_reset(self);
    free(self);
}

void gomocu_server_worker_release_state(gomocu_server_worker *self)
{
    semop(self->semaphore, release_semop, 1);
}


