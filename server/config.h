#pragma once
#include <stddef.h>

int gomocu_server_config_init();
void gomocu_server_config_write(const char* key, const char* value);
int gomocu_server_config_read(const char* key, char* value, size_t value_size);
