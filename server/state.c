#include "state.h"

#include <stdlib.h>
#include <string.h>

gomocu_server_state* gomocu_server_state_new() {
    gomocu_server_state* result = malloc(sizeof(gomocu_server_state));


    result->max_field_w = result->max_field_h = -1;
    result->turn_count = 0;
    result->player_one_rq[0] = result->player_one_rsp[0] = result->player_two_rq[0] = result->player_two_rsp[0] = '\0';
    result->selectedX = result->selectedY = -1;
    result->stage = gomocu_server_state_game_stage_stopped;

    return result;
}

void gomocu_server_state_turn(gomocu_server_state* self) {
    self->turn_count++;
}

int gomocu_server_state_set(gomocu_server_state* self, int x, int y, gomocu_server_state_cell_state state)
{
    if((x<0 || x>=self->max_field_w) && (y<0 || y>=self->max_field_h)){
        return 0;
    }

    if(self->field[y*self->max_field_w + x] != gomocu_server_state_cell_state_none){
        return 0;
    }

    self->field[y*self->max_field_w + x] = state;

    return 1;
}


gomocu_server_state_cell_state gomocu_server_state_get(gomocu_server_state* self, int x, int y) {
    if((x<0 || x>=self->max_field_w) && (y<0 || y>=self->max_field_h)){
        return gomocu_server_state_cell_state_invalid;
    }

    return self->field[y*self->max_field_w + x];
}

void gomocu_server_state_free(gomocu_server_state *self)
{
    free(self);
}
