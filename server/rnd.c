#include "rnd.h"

#include <string.h>

#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include "logger.h"

extern int errno;

void gomocu_rnd_next_bytes(uint8_t *bytes, size_t size)
{
    int rndfd = open("/dev/urandom", O_RDONLY);
    LOGGER_ASSERT_SYSCALL(rndfd, "Error opening /dev/urandom");

    read(rndfd, bytes, size);

    close(rndfd);
}
