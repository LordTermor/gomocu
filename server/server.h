#pragma once

struct _gomocu_server_state;
struct _gomocu_server_worker;
struct _gomocu_server_arbitrator;
struct _gomocu_server_generator;
struct _gomocu_server_executor;
struct _gomocu_server_master {
    struct _gomocu_server_arbitrator* arbitrator;
    struct _gomocu_server_generator* generator;
    struct _gomocu_server_executor* executor;

    char* endpoint;
    int exit;

    int fd1;
    int fd2;
    int serverfd;

    int semaphore;
    int shfd;

    char* gamename;

    struct _gomocu_server_state* state;
};

typedef struct _gomocu_server_master gomocu_server_master;

gomocu_server_master* gomocu_server_master_new(const char* endpoint);
void gomocu_server_master_init(gomocu_server_master* self);

void gomocu_server_master_reset(gomocu_server_master* self);
void gomocu_server_master_free(gomocu_server_master* self);

void gomocu_server_master_start(gomocu_server_master* self);
void gomocu_server_master_start_detached(gomocu_server_master* self);
void gomocu_server_master_stop(gomocu_server_master* self);
