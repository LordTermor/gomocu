#pragma once

struct _gomocu_server_state;

struct _gomocu_server_worker {

void (*start)(void* self);

struct _gomocu_server_state* state;
int shmemfd;
int semaphore;

};

typedef struct _gomocu_server_worker gomocu_server_worker;

gomocu_server_worker* gomocu_server_worker_new(int shmemfd, int semaphore);

void gomocu_server_worker_init(gomocu_server_worker* self,int shmemfd, int semaphore);

void gomocu_server_worker_reset(gomocu_server_worker* self);
void gomocu_server_worker_free(gomocu_server_worker* self);

void gomocu_server_worker_start(gomocu_server_worker* self);

struct _gomocu_server_state* gomocu_server_worker_acquire_state(gomocu_server_worker* self);
void gomocu_server_worker_release_state(gomocu_server_worker* self);

