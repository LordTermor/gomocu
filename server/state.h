#pragma once

#define FIELD_SIZE 15

enum _gomocu_server_state_cell_state{
   gomocu_server_state_cell_state_invalid,
   gomocu_server_state_cell_state_none,
   gomocu_server_state_cell_state_first,
   gomocu_server_state_cell_state_second
};
typedef enum _gomocu_server_state_cell_state gomocu_server_state_cell_state;

enum _gomocu_server_state_game_stage
{
   gomocu_server_state_game_stage_preparing,
    gomocu_server_state_game_stage_waitforsecondplayer,
    gomocu_server_state_game_stage_firstplayer_turn,
    gomocu_server_state_game_stage_secondplayer_turn,
   gomocu_server_state_game_stage_stopped,
    gomocu_server_state_game_stage_error,
};
typedef enum _gomocu_server_state_game_stage gomocu_server_state_game_stage;


struct _gomocu_server_state {


    gomocu_server_state_cell_state field[FIELD_SIZE*FIELD_SIZE];
    int max_field_w;
    int max_field_h;

    int turn_count;

    gomocu_server_state_game_stage stage;

    char player_one_rq[255];
    char player_two_rq[255];

    int selectedX;
    int selectedY;

    char player_one_rsp[255];
    char player_two_rsp[255];
};

typedef struct _gomocu_server_state gomocu_server_state;

gomocu_server_state* gomocu_server_state_new();
void gomocu_server_state_free(gomocu_server_state* self);
void gomocu_server_state_turn(gomocu_server_state* self);
int gomocu_server_state_set(gomocu_server_state* self, int x, int y, gomocu_server_state_cell_state state);
gomocu_server_state_cell_state gomocu_server_state_get(gomocu_server_state* self, int x, int y);
