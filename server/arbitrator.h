#pragma once

#include "worker.h"

union sem_t;

struct _gomocu_server_arbitrator {
    gomocu_server_worker parent_instance;
};

typedef struct _gomocu_server_arbitrator gomocu_server_arbitrator;

gomocu_server_arbitrator* gomocu_server_arbitrator_new(int shmemfd, int semaphore);
void gomocu_server_arbitrator_init(gomocu_server_arbitrator* self, int shmemfd, int semaphore);

void gomocu_server_arbitrator_free(gomocu_server_arbitrator* self);
