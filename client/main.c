#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <unistd.h>
#include <string.h>
#include <poll.h>

#define FIELD_SIZE 15

#define clear() printf("\033[H\033[J")


enum state {
    invalid = 0,
    none,
    first,
    second
};

enum state field[FIELD_SIZE*FIELD_SIZE];

const char* invalid_state_char = "!!";
const char* first_state_char = " X";
const char* second_state_char = " 0";

const char* state_to_char(enum state st){
    switch(st){
    case first:
        return first_state_char;
    case second:
        return second_state_char;
    case none:
        return "  ";
    default:
        return invalid_state_char;
    }
}

void draw_hline(){
    printf("├──┼──┼");
    for(int i =0;i<FIELD_SIZE-2;i++){
        printf("──┼");
    }
    printf("──┤\n");
}

void draw_field(){
    printf("┌──┬──┬");
    for(int i =0;i<FIELD_SIZE-2;i++){
        printf("──┬");
    }
    printf("──┐\n");

    printf("│ ╲│01│");
    for(int i =1;i<FIELD_SIZE-1;i++){
        printf("%.2d│", i+1);
    }
    printf("%.2d│\n",FIELD_SIZE);
    draw_hline();

    for(int j = 0;j<FIELD_SIZE-1;j++){
        const char* c = state_to_char(field[j*FIELD_SIZE]);

        printf("│%.2d│%.2s│", j+1, c);
        for(int i =1;i<FIELD_SIZE-1;i++){
            printf("%.2s│", state_to_char(field[j*FIELD_SIZE+i]));
        }
        c = state_to_char(field[j*FIELD_SIZE+FIELD_SIZE-1]);
        printf("%.2s│\n", c);
        draw_hline();
    }

    const char* c = state_to_char(field[(FIELD_SIZE-1)*FIELD_SIZE]);

    printf("│%.2d│%.2s│", FIELD_SIZE, c);
    for(int i =1;i<FIELD_SIZE-1;i++){
        printf("%.2s│", state_to_char(field[(FIELD_SIZE-1)*FIELD_SIZE+i]));
    }
    c = state_to_char(field[(FIELD_SIZE-1)*FIELD_SIZE+FIELD_SIZE-1]);
    printf("%.2s│\n", c);

    printf("└──┴──┴");
    for(int i =0;i<FIELD_SIZE-2;i++){
        printf("──┴");
    }
    printf("──┘\n");
}

int main(int argc, char* argv[]){

    for(int i = 0;i<FIELD_SIZE*FIELD_SIZE;i++){
        field[i] = none;
    }
    struct sockaddr_in addr = {0};
    addr.sin_family = AF_INET;
    addr.sin_port = htons(atoi(argv[2]));
    addr.sin_addr.s_addr = inet_addr(argv[1]);

    int sock = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sock == -1) {
        perror("Socket creation error");
        return EXIT_FAILURE;
    }
    if (connect(sock, (struct sockaddr*) &addr, sizeof(addr)) == -1) {
        perror("Connection error");
        close(sock);
        return EXIT_FAILURE;
    }


    int stdin_offset = 0;
    char stdin_buffer[1024];

    int socket_offset = 0;
    char socket_buffer[1024];

    printf("> ");
    fflush(stdout);

    while(1){

        struct pollfd input[2] = {
        {.fd = STDIN_FILENO, .events = POLLIN},
        {.fd = sock, .events = POLLIN}};

        int ret = poll(input, 2, -1);

        if(ret!=0){
            if ( input[0].revents & POLLIN ){
                input[0].revents = 0;
                int cmdlen = read(STDIN_FILENO,
                                  stdin_buffer + stdin_offset,
                                  1024 - stdin_offset);

                if(*(stdin_buffer+stdin_offset+cmdlen-1) == '\n'){

                    *(stdin_buffer+stdin_offset+cmdlen-1) = '\0';

                    if(send(sock, stdin_buffer, stdin_offset+cmdlen, 0)<=0){
                        printf("Error writing to socket. Aborting...");
                        _exit(1);
                    }

                    stdin_offset = 0;
                    stdin_buffer[0] = '\0';

                } else {
                    stdin_offset+=cmdlen;
                }

            }
            if(input[1].revents & POLLIN){
                input[1].revents = 0;
                int cmdlen = read(sock,
                                  socket_buffer + socket_offset,
                                  1024 - socket_offset);

                if(*(socket_buffer+socket_offset+cmdlen-1) == '\n'){
                    *(socket_buffer+socket_offset+cmdlen-1) = '\0';

                    if( strncmp(socket_buffer, "TURN ", strlen("TURN ")) == 0 ){
                        strtok(socket_buffer, " ");
                        char* splayer = strtok(NULL, " ");

                        char* sx = strtok(NULL, " ");
                        char* sy = strtok(NULL, " \n");

                        if(splayer==NULL || sx == NULL || sy == NULL){
                            printf("Server sent malformed TURN command, contact your system administrator");
                            continue;
                        }
                        char* lastchar = NULL;
                        int player = strtol(splayer, &lastchar, 10);
                        int x = strtol(sx, &lastchar, 10);
                        int y = strtol(sy, &lastchar, 10);

                        field[y*FIELD_SIZE+x] = player==1?first:second;

                        clear();
                        draw_field();
                        printf("> %*s", stdin_offset, stdin_buffer);

                    } else if(strncmp(socket_buffer, "END", strlen("END"))==0) {
                        printf("\nServer ended your session! Exiting...\n");
                        _exit(0);
                    } else {
                        printf("\nServer: %s\n>%*s", socket_buffer,stdin_offset,stdin_buffer);
                        fflush(stdout);
                    }
                } else {
                    stdin_offset += cmdlen;
                }

            }

        }
    }
}
